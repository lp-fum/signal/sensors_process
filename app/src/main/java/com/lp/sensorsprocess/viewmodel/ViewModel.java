package com.lp.sensorsprocess.viewmodel;

import android.hardware.Sensor;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ViewModel extends androidx.lifecycle.ViewModel {
    String TAG = "viewmodelSaving";
    long lastFlushTime = -1;
    int maxCapacity = 10000;
    int minCapacity = 100;
    int minTimeInSeconds = 5;
    String path = "";

    ArrayList<Data> temporaryArrayList = new ArrayList<>();


    public ViewModel() {

    }

    public void setApplicationPath(String path) {
        this.path = path;
        Log.d(TAG, "setApplicationPath: " + this.path);
    }

    public void storeData(int sensorType, long time, float[] data, String sensorName) {
        Log.d(TAG, "storeData: " + data);
        if (this.lastFlushTime < 0) this.lastFlushTime = time;
        Data tempData = new Data(sensorType, time, data, sensorName);
        this.temporaryArrayList.add(tempData);
        if (this.temporaryArrayList.size() > minCapacity) {
            if ((time - this.lastFlushTime) / 1000000000 > this.minTimeInSeconds) flush(time);
            else if (this.temporaryArrayList.size() > maxCapacity) flush(time);
        }
    }


    private void flush(long time) {
        new Store().execute(this.temporaryArrayList, this.path);
        this.temporaryArrayList = new ArrayList<>();
        this.lastFlushTime = time;
        Log.d(TAG, "flush: ");
    }

    public void stop() {
        flush(-1);
    }


    private static class Store extends AsyncTask {
        String TAG = "viewmodelSaving";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "onPreExecute: ");
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                String fileName = "data";
                ArrayList<Data> temporaryArrayList = (ArrayList<Data>) objects[0];
                String path = (String) objects[1];
                File dir = new File(path);
                for (int i = 0; i < dir.listFiles().length + 1; i++) {
                    boolean sw = false;
                    for (File file : dir.listFiles()) {
                        if (file.isFile()) {
                            if (file.getName().contains(fileName + String.valueOf(i))) {
                                sw = true;
                                break;
                            }
                        }
                    }
                    if (!sw) {
                        fileName += String.valueOf(i);
                        break;
                    }
                }


                StringBuilder content = new StringBuilder();


                for (Data data : temporaryArrayList) {
                    content.append(data.getSensorType()).append(",").append(data.getName()).append(",").append(data.time);
                    for (float i : data.getData()) {
                        content.append("," + i);
                    }
                    content.append("\n");
                }

                try {
                    File file = new File(path + fileName + ".csv");
                    // if file doesnt exists, then create it
                    if (!file.exists()) {
                        file.createNewFile();
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write("Sensor Type,Sensor Name,Time,Data1,Data2,Data3,Data4,Data5\n");
                        bw.close();

                    }

                    FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(content.toString());
                    bw.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }


                return 1;
            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (o.equals(null)) {
                System.err.println("something went wrong");
                // TODO: 11/24/20 some exception handler is required...
            }
        }
    }

}

class Data {
    int sensorType = 0;
    String name;
    long time = 0;
    float data[];


    public Data(int sensorType, long time, float[] data, String name) {
        this.time = time;
        this.sensorType = sensorType;
        this.data = data;
        this.name = name;
    }

    public float[] getData() {
        return data;
    }

    public int getSensorType() {
        return sensorType;
    }

    public String getName() {
        return name;
    }

    public long getTime() {
        return time;
    }
}