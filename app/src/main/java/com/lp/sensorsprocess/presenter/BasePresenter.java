package com.lp.sensorsprocess.presenter;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import com.lp.sensorsprocess.model.network.rest.ErrorEntity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class BasePresenter<T> {
    public final Activity activity;
    ArrayList<View> lottieAnimationViews;
    ArrayList<View> views;
    final protected WeakReference<T> mView;
    private boolean isLoading;


    int page;
    private int pageNumber = 0;

    public boolean isLoading() {
        return isLoading;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int i) {
        this.page = i;
    }

    public BasePresenter(T view, Activity activity, ArrayList<View> lottieAnimationViews, ArrayList<View> views) {
        this.lottieAnimationViews = lottieAnimationViews;
        this.views = views;
        this.activity = activity;
        isLoading = false;
        this.mView = new WeakReference<>(view);
        page = 1;
    }

    public BasePresenter(T view, Activity activity, View lottieAnimationView, ArrayList<View> views) {
        this.lottieAnimationViews = new ArrayList<>();
        this.lottieAnimationViews.add(lottieAnimationView);
        isLoading = false;

        this.views = views;
        this.activity = activity;
        this.mView = new WeakReference<>(view);
        page = 1;
    }


    public BasePresenter(T view, Activity activity, View lottieAnimationView, View views) {
        this.lottieAnimationViews = new ArrayList<>();
        this.lottieAnimationViews.add(lottieAnimationView);
        this.views = new ArrayList<>();
        this.views.add(views);
        isLoading = false;

        this.activity = activity;
        this.mView = new WeakReference<>(view);
        if (lottieAnimationViews == null) {
            this.lottieAnimationViews = null;
        }
        if (views == null) {
            this.views = null;
        }
        page = 1;
    }

    protected boolean isViewAvailable() {
        return this.mView != null && this.mView.get() != null;
    }

    public void detachView() {
        this.mView.clear();
    }

    public void showMsg(String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    public void showError(ErrorEntity errorEntity) {
        Toast.makeText(activity, errorEntity.getExceptionMessage(), Toast.LENGTH_SHORT).show();
    }

    public void showError(String errorEntity) {
        Toast.makeText(activity, errorEntity, Toast.LENGTH_SHORT).show();
    }

    public void startProgress() {
        if (!isLoading) {
            isLoading = true;
            if (lottieAnimationViews != null) {
                for (int i = 0; i < lottieAnimationViews.size(); i++)
                    lottieAnimationViews.get(i).setVisibility(View.VISIBLE);
            }
            if (views != null) {
                for (int i = 0; i < views.size(); i++)
                    views.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    public void stopProgress() {
        if (isLoading) {
            isLoading = false;
            if (lottieAnimationViews != null) {
                for (int i = 0; i < lottieAnimationViews.size(); i++)
                    lottieAnimationViews.get(i).setVisibility(View.INVISIBLE);
            }
            if (views != null) {
                for (int i = 0; i < views.size(); i++)
                    views.get(i).setVisibility(View.VISIBLE);
            }
        }
    }
}
