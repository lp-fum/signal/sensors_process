package com.lp.sensorsprocess.model.database;

import androidx.room.Dao;

@Dao
public interface AppDao {

//    //region page category1
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCategories1(ArrayList<PageCategory1> category1s);
//
//    @Query("SELECT * FROM pageCategory1")
//    List<PageCategory1> loadAllCategories1();
//
//    @Delete
//    void delete(PageCategory1 user);
//
//    //endregion
//
//    //region page category2
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCategories2(ArrayList<PageCategory2> category2s);
//
//    @Query("SELECT * FROM pageCategory2")
//    PageCategory2[] loadAllCategories2();
//
//    @Query("select * from pageCategory2 where id =:id")
//    List<PageCategory2> loadCategoryByParent2(int id);
//
//    @Delete
//    void delete(PageCategory2 user);
//
//    //endregion
//
//    //region page category3
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCategories3(ArrayList<PageCategory3> category3s);
//
//    @Query("SELECT * FROM pageCategory3")
//    PageCategory3[] loadAllCategories3();
//
//    @Query("select * from pageCategory3 where id =:id")
//    List<PageCategory3> loadCategoryByParent3(int id);
//
//    @Delete
//    void delete(PageCategory3 user);
//
//    //endregion
//
//    //region page category4
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCategories4(ArrayList<PageCategory4> category4s);
//
//    @Query("SELECT * FROM pageCategory4")
//    PageCategory4[] loadAllCategories4();
//
//    @Query("select * from pageCategory4 where id =:id")
//    List<PageCategory4> loadCategoryByParent4(Integer id);
//
//    @Delete
//    void delete(PageCategory4 user);
//
//    //endregion
//
//    //region page category5
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCategories5(ArrayList<PageCategory5> category5s);
//
//    @Query("SELECT * FROM pageCategory5")
//    PageCategory5[] loadAllCategories5();
//
//    @Query("select * from pageCategory5 where id =:id")
//    List<PageCategory5> loadCategoryByParent5(int id);
//
//    @Delete
//    void delete(PageCategory5 user);
//
//    //endregion
//
//    //region country
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCoutries(ArrayList<Country> countries);
//
//    @Query("select * from country")
//    List<Country> getAllCountries();
//
//    //endregion
//
//    //region province
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllProvinces(ArrayList<Province> provinces);
//
//    @Query("select * from province")
//    Province[] getAllProvinces();
//    //endregion
//
//    //region city
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCities(ArrayList<City> cities);
//
//    @Query("select * from city")
//    List<City> getAllCities();
//    //endregion
//
//    //region currency
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllCurrencies(ArrayList<Currency> cities);
//
//    @Query("select * from currency")
//    List<Country> getAllCurrencies();
//    //endregion
//
//    //region area
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertAllAreas(ArrayList<Area> areas);
//
//    @Query("SELECT * FROM area")
//    List<Area> loadAllAreas();
//
//    @Delete
//    void deleteArea(Area area);
//    //endregion
}
