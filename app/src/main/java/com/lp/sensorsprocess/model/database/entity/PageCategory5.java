package com.lp.sensorsprocess.model.database.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "pageCategory5")
public class PageCategory5 {

    @PrimaryKey
    @SerializedName("id")
    public int id;
    @SerializedName("title")
    public String title;
    @SerializedName("objectNameId")
    public int objectNameId;
    @SerializedName("isAllowed")
    public boolean isAllowed;
    @SerializedName("tb_cat1")
    public int category1Id;
    @SerializedName("tb_cat2")
    public int category2Id;
    @SerializedName("tb_cat3")
    public int category3Id;
    @SerializedName("tb_cat4")
    public int category4Id;

    @Ignore
    public int position;

    public int getId() {
        return id;
    }

    public PageCategory5 setId(int id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public PageCategory5 setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getObjectNameId() {
        return objectNameId;
    }

    public PageCategory5 setObjectNameId(int objectNameId) {
        this.objectNameId = objectNameId;
        return this;
    }

    public boolean isAllowed() {
        return isAllowed;
    }

    public PageCategory5 setAllowed(boolean allowed) {
        isAllowed = allowed;
        return this;
    }

    public int getCategory1Id() {
        return category1Id;
    }

    public PageCategory5 setCategory1Id(int category1Id) {
        this.category1Id = category1Id;
        return this;
    }

    public int getCategory2Id() {
        return category2Id;
    }

    public PageCategory5 setCategory2Id(int category2Id) {
        this.category2Id = category2Id;
        return this;
    }

    public int getCategory3Id() {
        return category3Id;
    }

    public PageCategory5 setCategory3Id(int category3Id) {
        this.category3Id = category3Id;
        return this;
    }

    public int getCategory4Id() {
        return category4Id;
    }

    public PageCategory5 setCategory4Id(int category4Id) {
        this.category4Id = category4Id;
        return this;
    }

    public int getPosition() {
        return position;
    }

    public PageCategory5 setPosition(int position) {
        this.position = position;
        return this;
    }
}
