package com.lp.sensorsprocess.model.dataProcess;

public class Statistics {

    //واریانس
    public static double variance(double dataArray[]) {
        double sum = 0;
        for (int i = 0; i < dataArray.length; i++)
            sum += dataArray[i];
        double mean = (double) sum / (double) dataArray.length;
        double sqDiff = 0;
        for (int i = 0; i < dataArray.length; i++)
            sqDiff += (dataArray[i] - mean) *
                    (dataArray[i] - mean);
        return (double) sqDiff / dataArray.length;
    }

    //انحراف معیار
    public static double standardDeviation(double dataArray[]) {
        return Math.sqrt(variance(dataArray));
    }

    //میانگین
    public static double average(double dataArray[]){
        int sum=0;
        for (int i=0;i<dataArray.length;i++){
            sum+=dataArray[i];
        }
        return (double) sum/dataArray.length;
    }

    public static double getMax(double dataArray[]){
        double max=Double.MIN_VALUE;
        for (int i=0;i<dataArray.length;i++){
            if (dataArray[i]>max){
                max=dataArray[i];
            }
        }
        return max;
    }

    public static double getMin(double dataArray[]){
        double min=Double.MAX_VALUE;
        for (int i=0;i<dataArray.length;i++){
            if (dataArray[i]<min){
                min=dataArray[i];
            }
        }
        return min;
    }

}
