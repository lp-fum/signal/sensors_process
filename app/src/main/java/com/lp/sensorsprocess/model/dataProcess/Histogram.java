package com.lp.sensorsprocess.model.dataProcess;

import java.util.ArrayList;
import java.util.List;

public class Histogram {
    private int upperBound,lowerBound;
    private List<HistData> histogramData;
    Histogram(int lowerBound,int upperBound,int numberOfBins,double[] data){
        this.upperBound=upperBound;
        this.lowerBound=lowerBound;
        histogramData=new ArrayList<>();
        prepareData(data,numberOfBins);
    }

    private void prepareData(double[] data,int numberOfBins){
        double step=(upperBound-lowerBound)/(double)numberOfBins;
        for (int i = 0; i < numberOfBins; i++) {
            histogramData.add(new HistData((step/2)*(2*i+1)));
        }
        for (int i = 0; i < data.length; i++) {
            histogramData.get(closestIndicator(data[i])).addFrequency();
        }
    }

    private int closestIndicator(double data){
        int i=0;
        double dist1,dist2;
        do {
            dist1=Math.abs(data-histogramData.get(i).indicator);
            dist2=Math.abs(data-histogramData.get(i+1).indicator);
            i++;
        }while (dist1>dist2 && i<histogramData.size()-1);
        if (i==histogramData.size()-1 && dist1>dist2) {
            return i;
        }
        return --i;
    }

    private class HistData{
        int frequency;
        double indicator;

        HistData(double indicator){
            this.indicator=indicator;
            this.frequency=0;
        }

        public void addFrequency(){
            frequency++;
        }
    }
}
