package com.lp.sensorsprocess.model.sensors;

import java.util.ArrayList;

public class SensorLp {
    private int sensorId;
    private String name;
    public ArrayList<ArrayList<Float>> data;
    public static final int MULTI_SENSOR_ID=-123456;

    public SensorLp(int sensorId, int countSensorId) {
        this.sensorId = sensorId;
        data = new ArrayList<>(countSensorId);
        for (int i = 0; i < countSensorId; i++) {
            data.add(new ArrayList<>());
        }
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
