package com.lp.sensorsprocess.model.network.rest;


import com.lp.sensorsprocess.model.network.api.Api;
import com.lp.sensorsprocess.model.network.api.ApiClient;

public class MethodApi {
    private static MethodApi instance;
    private Api api;

    public MethodApi() {
        api = ApiClient.getLoginClient().create(Api.class);
    }


    public static MethodApi getInstance() {
        if (instance == null)
            instance = new MethodApi();

        return instance;
    }

    public Api getApi() {
        return api;
    }

    public void setApi(Api api) {
        this.api = api;
    }

//    //region login
//    public void sendSensorData(SendSmsForSignUp entity, final IRemoteCallback<String> callback) {
//        final Call<String> call = api.sendSmsForSignUp(entity);
//        call.enqueue(new Enqueue<>(new IRemoteCallback<String>() {
//            @Override
//            public void onResponse(Boolean answer) {
//                callback.onResponse(answer);
//            }
//
//            @Override
//            public void onSuccess(String result) {
//                callback.onSuccess(result);
//            }
//
//            @Override
//            public void onFail(ErrorEntity errorObject) {
//                callback.onFail(errorObject);
//            }
//
//            @Override
//            public void onFinish(Boolean answer, boolean connection) {
//                callback.onFinish(answer, connection);
//            }
//        }));
//    }


}
