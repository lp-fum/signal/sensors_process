package com.lp.sensorsprocess.model.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.sensorsprocess.R;
import com.lp.sensorsprocess.model.sensors.SensorLp;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SensorAdapter extends RecyclerView.Adapter<SensorAdapter.SensorViewHolder> {
    ArrayList<SensorModel> sensorModels;
    OnItemClickListener itemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        itemClickListener = listener;
    }

    @NonNull
    @Override
    public SensorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        SensorViewHolder sensorViewHolder = new SensorViewHolder(v, itemClickListener);
        return sensorViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SensorViewHolder holder, int position) {
        SensorModel sensorModel = sensorModels.get(position);
        holder.title.setText(sensorModel.getTitle());
        holder.details.setText(sensorModel.getDetails());
        holder.imageView.setImageResource(sensorModel.getImageView());
        holder.switchCompat.setVisibility(sensorModel.getVisibility());
        holder.switchCompat.setChecked(sensorModel.getSelect());

    }

    @Override
    public int getItemCount() {
        return sensorModels.size();
    }

    public SensorAdapter(ArrayList<SensorModel> sensorModel) {
        sensorModels = sensorModel;
    }

    public static class SensorViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imageView;
        public TextView title;
        public TextView details;
        public SwitchCompat switchCompat;

        public SensorViewHolder(@NonNull View itemView, OnItemClickListener itemClickListener) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image1);
            title = itemView.findViewById(R.id.text1);
            details = itemView.findViewById(R.id.text2);
            switchCompat=itemView.findViewById(R.id.switch1);
            itemView.setOnClickListener(v -> {
                if (itemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        itemClickListener.onItemClick(position);
                    }
                }
            });
        }
    }
}
