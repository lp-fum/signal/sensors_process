package com.lp.sensorsprocess.model.network.rest;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.GATEWAY_TIMEOUT;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.INTERNAL_SERVER_ERROR;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.METHOD_NOT_ALLOWED;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.NOT_ACCEPTABLE;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.NOT_FOUND;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.SERVICE_UNAVAILABLE;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.SOCKET_TIMEOUT;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.TIMEOUT;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.UNARUTHORIZED;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.UNKNOWN;
import static com.lp.sensorsprocess.model.network.rest.InteractorResponse.UNSUPPORTED_MEDIA_TYPE;


public class Enqueue<T> implements Callback<T> {
    private final com.lp.sensorsprocess.model.network.rest.IRemoteCallback<T> callback;
    public static final String TAG = Enqueue.class.getSimpleName();

    public Enqueue(com.lp.sensorsprocess.model.network.rest.IRemoteCallback<T> callback) {
        this.callback = callback;
    }

    public void onResponse(@NotNull Call<T> call, Response<T> response) {
        if (!response.isSuccessful()) {
            String data = "data";
            try {
                data = response.errorBody().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i(TAG, "onResponse: " + data);
            this.callback.onResponse(false);
            this.callback.onFail(this.parseError(response, data));
            this.callback.onFinish(false, true);
        } else {
            this.callback.onResponse(true);
            this.callback.onSuccess(response.body());
            this.callback.onFinish(true, true);
        }
    }

    private ErrorEntity parseError(Response<T> response, String dataError) {
        ErrorEntity error = new ErrorEntity();
        if (response.body() instanceof SocketTimeoutException) {
            error.setStatus("Error");
            error.setUiErrorMessage("Socket TimeOut UiError Msg");
            error.setExceptionMessage("Socket TimeOut Error Msg");
            error.setInteractorResponse(SOCKET_TIMEOUT);
        } else if (response.body() instanceof TimeoutException) {
            error.setStatus("Error");
            error.setUiErrorMessage("TimeOut UiError Msg");
            error.setExceptionMessage("TimeOut Error Msg");
            error.setInteractorResponse(TIMEOUT);
        } else {
            error.setUiErrorMessage(dataError);
            switch (response.code()) {
                case 400:
                    error.setInteractorResponse(InteractorResponse.BAD_REQUEST);
                    break;
                case 401:
                    error.setInteractorResponse(UNARUTHORIZED);
                    break;
                case 403:
                    error.setInteractorResponse(InteractorResponse.FORBIDDEN);
                    break;
                case 404:
                    error.setInteractorResponse(NOT_FOUND);
                    break;
                case 405:
                    error.setInteractorResponse(METHOD_NOT_ALLOWED);
                    break;
                case 406:
                    error.setInteractorResponse(NOT_ACCEPTABLE);
                    break;
                case 415:
                    error.setInteractorResponse(UNSUPPORTED_MEDIA_TYPE);
                    break;
                case 500:
                    error.setInteractorResponse(INTERNAL_SERVER_ERROR);
                    break;
                case 502:
                    error.setInteractorResponse(InteractorResponse.BAD_GATEWAY);
                    break;
                case 503:
                    error.setInteractorResponse(SERVICE_UNAVAILABLE);
                    break;
                case 504:
                    error.setInteractorResponse(GATEWAY_TIMEOUT);
                    break;
                default:
                    error.setInteractorResponse(UNKNOWN);
                    break;
            }
        }
        return error;
    }

    public void onFailure(@NotNull Call<T> call, @NotNull Throwable t) {
        this.callback.onResponse(false);
        if (t instanceof SocketTimeoutException) {
            ErrorEntity error = new ErrorEntity();
            error.setStatus("Error");
            error.setUiErrorMessage("Socket TimeOut UiError Msg");
            error.setExceptionMessage("Socket TimeOut Error Msg");
            error.setInteractorResponse(SOCKET_TIMEOUT);
            this.callback.onFail(error);
            this.callback.onFinish(false, false);
        } else if (t instanceof TimeoutException) {
            ErrorEntity error = new ErrorEntity();
            error.setStatus("Error");
            error.setUiErrorMessage("TimeOut UiError Msg");
            error.setExceptionMessage("TimeOut Error Msg");
            error.setInteractorResponse(TIMEOUT);
            this.callback.onFail(error);
            this.callback.onFinish(false, false);
        } else {
            this.callback.onFinish(false, false);
        }
    }
}