package com.lp.sensorsprocess.model.adapters;

import com.lp.sensorsprocess.model.sensors.SensorLp;

public class SensorModel {
    private int imageView;
    private String title;
    private String details;
    private int sensorId;
    private boolean select;
    private int visibility = 0;

    public SensorModel(int imageView, String title, String details, int sensorId, boolean select) {
        this.imageView = imageView;
        this.title = title;
        this.details = details;
        this.sensorId = sensorId;
        this.select = select;
        if (sensorId == SensorLp.MULTI_SENSOR_ID) {
            visibility = 4;
        }
    }

    public int getImageView() {
        return imageView;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }

    public boolean getSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }
}
