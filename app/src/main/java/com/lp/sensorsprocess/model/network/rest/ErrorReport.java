package com.lp.sensorsprocess.model.network.rest;

public class ErrorReport {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
