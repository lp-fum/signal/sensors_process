package com.lp.sensorsprocess.model.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.lp.sensorsprocess.model.database.entity.PageCategory5;


@Database(entities = {

        PageCategory5.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {

    public abstract AppDao appDao();
}
