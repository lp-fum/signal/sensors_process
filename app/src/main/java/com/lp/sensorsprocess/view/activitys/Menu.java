package com.lp.sensorsprocess.view.activitys;

import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.InputType;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lp.sensorsprocess.R;
import com.lp.sensorsprocess.model.adapters.SensorAdapter;
import com.lp.sensorsprocess.model.adapters.SensorModel;
import com.lp.sensorsprocess.model.sensors.SensorLp;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Menu extends AppCompatActivity {
    @BindView(R.id.recyclerView_sensors)
    RecyclerView recyclerView;
    ArrayList<SensorModel> sensors = new ArrayList<>();
    private SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        initRecycleView();

    }

    void initRecycleView() {
        initArrayList();
        recyclerView.setHasFixedSize(true);
        SensorAdapter adapter = new SensorAdapter(sensors);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(position -> {
            Intent intent;
            if (sensors.get(position).getSensorId() == SensorLp.MULTI_SENSOR_ID) {
                intent = new Intent(this, MultiSensor.class);
                ArrayList<Integer> integers = new ArrayList<>();
                for (int i = 0; i < sensors.size(); i++) {
                    if (sensors.get(i).getSelect() && sensors.get(i).getSensorId() != SensorLp.MULTI_SENSOR_ID) {
                        integers.add(sensors.get(i).getSensorId());
                    }
                }
                if (integers.size() == 0) {
                    Toast.makeText(this, "Please choose at least one sensor", Toast.LENGTH_LONG).show();
                    return;
                }
                intent.putIntegerArrayListExtra("sensor_ids", integers);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Please enter frequency 1 - 100");

// Set up the input
                final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                builder.setView(input);

// Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Integer.parseInt(input.getText().toString()) < 100 & Integer.parseInt(input.getText().toString()) > 1) {
                            intent.putExtra("frequency", Double.valueOf(input.getText().toString()));
                            Toast toast = Toast.makeText(getApplicationContext(), "Your Input Is Not Valid Defualt Value Is 60", Toast.LENGTH_LONG);
                            toast.show();
                        } else
                            intent.putExtra("frequency", Double.valueOf(60.00));

                        startActivity(intent);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                return;
            }
            sensors.get(position).setSelect(!sensors.get(position).getSelect());
            adapter.notifyItemChanged(position);

        });
    }

    private void initArrayList() {
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null)
            sensors.add(new SensorModel(R.drawable.ratation_vector, "Rotation Vector", "details for rotation", Sensor.TYPE_ROTATION_VECTOR, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null)

            sensors.add(new SensorModel(R.drawable.proximity, "Proximity", "details for", Sensor.TYPE_PROXIMITY, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) != null)

            sensors.add(new SensorModel(R.drawable.pressure, "Pressure", "details for", Sensor.TYPE_PRESSURE, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null)

            sensors.add(new SensorModel(R.drawable.light, "Light", "details for", Sensor.TYPE_LIGHT, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null)

            sensors.add(new SensorModel(R.drawable.gyrosco, "Gyroscope", "details for", Sensor.TYPE_GYROSCOPE, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null)

            sensors.add(new SensorModel(R.drawable.humity, "Ambient Temperature", "details for", Sensor.TYPE_AMBIENT_TEMPERATURE, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)

            sensors.add(new SensorModel(R.drawable.acc, "Accelerometer", "details for", Sensor.TYPE_ACCELEROMETER, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null)

            sensors.add(new SensorModel(R.drawable.magnatic, "Magnetic Field", "details for", Sensor.TYPE_MAGNETIC_FIELD, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null)

            sensors.add(new SensorModel(R.drawable.gravity, "Gravity", "details for", Sensor.TYPE_GRAVITY, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION) != null)

            sensors.add(new SensorModel(R.drawable.orien, "Orientation", "details for", Sensor.TYPE_ORIENTATION, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY) != null)

            sensors.add(new SensorModel(R.drawable.humity, "Humidity", "details for", Sensor.TYPE_RELATIVE_HUMIDITY, false));
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null)

            sensors.add(new SensorModel(R.drawable.acc, "Linear Acceleration", "details for", Sensor.TYPE_LINEAR_ACCELERATION, false));

        sensors.add(new SensorModel(R.drawable.start, "START", "start recording selected sensors", SensorLp.MULTI_SENSOR_ID, false));

    }

}