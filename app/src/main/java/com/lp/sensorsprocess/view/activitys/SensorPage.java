package com.lp.sensorsprocess.view.activitys;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.lp.sensorsprocess.R;
import com.lp.sensorsprocess.model.sensors.SensorLp;
import com.lp.sensorsprocess.viewmodel.ViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SensorPage extends AppCompatActivity implements SensorEventListener {
    @BindView(R.id.sensorTitle)
    TextView sensorTitle;
    @BindView(R.id.sensorImage)
    CircleImageView circleImageView;
    private SensorManager sensorManager;
    private int sensorId = -1;
    private String TAG = "sensorPage";
    private int countSensorId = 0;
    private SensorLp sensorLp;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_page);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        ButterKnife.bind(this);
        initExtra();
        getData();

    }

    private void getData() {
        startCollecting(sensorId);

    }

    void initExtra() {

        int imageID = getIntent().getIntExtra("image_id", 0);
        circleImageView.setImageResource(imageID);
        String sensorName = getIntent().getStringExtra("sensor_name");
        sensorTitle.setText(sensorName);
        sensorId = getIntent().getIntExtra("sensor_id", -1);
        getCountSensorData();
        sensorLp = new SensorLp(sensorId, countSensorId);
        Toast.makeText(this, "Gathering Data is start", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
//        System.out.println(event.values.length);
//        for (int i = 0; i < event.values.length; i++) {
//            sensorLp.data.get(i).add(event.values[i]);
//        }
//        System.out.println(sensorLp.data.toString());
        Log.d(TAG, "onSensorChanged: ");
        viewModel.storeData(event.sensor.getType(),event.timestamp,event.values,event.sensor.getName());
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    public void startCollecting(int sensorType) {
        SensorManager sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(sensorType),
                SensorManager.SENSOR_DELAY_NORMAL);
        this.setSensorManager(sensorManager);

    }

    public void endCollecting(int sensorType) {
        this.sensorManager.unregisterListener(this, this.sensorManager.getDefaultSensor(sensorType));
        this.setSensorManager(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        endCollecting(sensorId);
        this.sensorManager = null;
        sensorLp = null;
        Toast.makeText(this, "Gathering is Stop", Toast.LENGTH_SHORT).show();

    }

    public void setSensorManager(SensorManager sensorManager) {
        this.sensorManager = sensorManager;
    }

    void getCountSensorData() {
        switch (sensorId) {
            case Sensor.TYPE_ACCELEROMETER:
            case Sensor.TYPE_GRAVITY:
            case Sensor.TYPE_GYROSCOPE:
            case Sensor.TYPE_LINEAR_ACCELERATION:
            case Sensor.TYPE_ORIENTATION:
            case Sensor.TYPE_MAGNETIC_FIELD:
                countSensorId = 3;
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
            case Sensor.TYPE_LIGHT:
            case Sensor.TYPE_PRESSURE:
            case Sensor.TYPE_PROXIMITY:
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                countSensorId = 1;
            case Sensor.TYPE_ROTATION_VECTOR:
                countSensorId = 5;

        }


    }


    public SensorLp getSensorLp() {
        return sensorLp;
    }

    public void setSensorLp(SensorLp sensorLp) {
        this.sensorLp = sensorLp;
    }
}