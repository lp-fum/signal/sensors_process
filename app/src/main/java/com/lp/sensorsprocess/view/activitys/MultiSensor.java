package com.lp.sensorsprocess.view.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.lp.sensorsprocess.R;
import com.lp.sensorsprocess.model.sensors.SensorLp;
import com.lp.sensorsprocess.viewmodel.ViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MultiSensor extends AppCompatActivity implements SensorEventListener {
    private ViewModel viewModel;
    Button button;
    private SensorLp[] sensorLps;
    private SensorManager sensorManager;
    private int[] sensorIds;
    private long lastSave = 0;
    private double FREQUENCY, AVGFrequency = 0;
    private final int SAMPLE_STEP = 30, SAMPLE_SIZE = 10;
    private final double NANO_TO_SECOND = 1000000000;
    private int eventCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_sensor);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        viewModel.setApplicationPath(getApplicationContext().getExternalFilesDir(null).getAbsolutePath() + "/");
        initExtra();
        init(sensorIds);
        FREQUENCY = 60;// to Hz and maximun acceptable value is 100
        lastSave = System.nanoTime();
        startCollection(sensorIds);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        endCollecting(sensorIds);
        this.sensorManager = null;
        sensorLps = null;
        viewModel.stop();
        Toast.makeText(this, "Gathering data has been stopped correctly", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        setLPSensorData(event, sensorLps[findSensor(sensorLps, event.sensor.getType())]);
//        if ((event.timestamp-lastSave)/NANO_TO_SECOND>=(1/FREQUENCY)) {
//            Log.d("freq test", "delta time: "+(float)(event.timestamp-lastSave)/1000000000+"\n freq: "+"FREQ: "+(double)1000000000/(event.timestamp-lastSave));
//            lastSave=event.timestamp;
//            viewModel.storeData(event.sensor.getType(), event.timestamp, event.values);
//        }
        viewModel.storeData(event.sensor.getType(), event.timestamp, event.values,event.sensor.getName());

        /*long deltaTime;
        double currFreq;
        deltaTime = (event.timestamp - lastSave);
        currFreq = NANO_TO_SECOND / deltaTime;
        if (currFreq <= FREQUENCY) {
            eventCount++;
            viewModel.storeData(event.sensor.getType(), event.timestamp, event.values,event.sensor.getName());
            if (eventCount % SAMPLE_STEP < SAMPLE_SIZE) {
                System.out.println();
                AVGFrequency += currFreq;
                System.out.println("test AVG:" + AVGFrequency);
                System.out.println("delta time: " + deltaTime);
            } else if (eventCount % SAMPLE_STEP == SAMPLE_SIZE) {
                AVGFrequency /= SAMPLE_SIZE;
                System.out.println("AVG freq now is: " + AVGFrequency);
                AVGFrequency = 0;
            }
            lastSave = event.timestamp;
        }*/
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void setLPSensorData(SensorEvent event, SensorLp sensorLp) {
        System.out.println("value.lenght: " + event.values.length + " type: " + sensorLp.getSensorId());
        for (int i = 0; i < event.values.length; i++) {
            sensorLp.data.get(i).add(event.values[i]);
        }
        System.out.println(sensorLp.data.toString());
    }

    private int getCountSensorData(int sensorId) {
        int countSensorId = -1;
        switch (sensorId) {
            case Sensor.TYPE_ACCELEROMETER:
            case Sensor.TYPE_GRAVITY:
            case Sensor.TYPE_GYROSCOPE:
            case Sensor.TYPE_LINEAR_ACCELERATION:
            case Sensor.TYPE_ORIENTATION:
            case Sensor.TYPE_MAGNETIC_FIELD:
                return 3;
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
            case Sensor.TYPE_LIGHT:
            case Sensor.TYPE_PRESSURE:
            case Sensor.TYPE_PROXIMITY:
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                return 1;
            case Sensor.TYPE_ROTATION_VECTOR:
                return 5;
        }
        return countSensorId;
    }

    private void init(int[] sensorIds) {
        sensorLps = new SensorLp[sensorIds.length];
        for (int i = 0; i < sensorIds.length; i++) {
            sensorLps[i] = new SensorLp(sensorIds[i], getCountSensorData(sensorIds[i]));
        }
    }

    private int findSensor(SensorLp[] sensors, int type) {
        for (int i = 0; i < sensors.length; i++) {
            if (sensors[i].getSensorId() == type) {
                return i;
            }
        }
        return -1;
    }

    private void startCollection(int[] sensorIds) {
        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        for (int i = 0; i < sensorIds.length; i++) {
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(sensorIds[i]),
                    SensorManager.SENSOR_DELAY_FASTEST);
        }
    }

    private void endCollecting(int[] sensorIds) {
        for (int i = 0; i < sensorIds.length; i++) {
            sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(sensorIds[i]));
        }
        this.sensorManager = null;
    }

    void initExtra() {
        ArrayList<Integer> integers = getIntent().getIntegerArrayListExtra("sensor_ids");
        int[] sensorIDs = new int[integers.size()];
        for (int i = 0; i < integers.size(); i++) {
            sensorIDs[i] = integers.get(i);
        }
        FREQUENCY=getIntent().getDoubleExtra("frequency",60);
        sensorIds = sensorIDs;

    }
}